package net.sppan.blog.controller.admin;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import net.sppan.blog.annotation.RequiresPermissions;
import net.sppan.blog.controller.BaseController;
import net.sppan.blog.model.Tags;
import net.sppan.blog.model.base.Condition;
import net.sppan.blog.model.base.Operators;

import com.jfinal.aop.Clear;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;

@RequiresPermissions(key={"admin:content:tags"})
public class AdminTagsController extends BaseController {
	
	@RequiresPermissions(key={"admin:content:tags:list"})
	public void list(){
		int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 10);
        String search = getPara("search");
        LinkedHashMap<String, String> orderby = new LinkedHashMap<String, String>();
        orderby.put("id", "asc");
        Set<Condition> conditions=new HashSet<Condition>();
        if(StrKit.notBlank(search)){
        	conditions.add(new Condition("tag_name", Operators.LIKE, search));
        	setAttr("search", search);
        }
		Page<Tags> page = Tags.dao.getPage(pageNumber, pageSize, conditions, orderby);
		setAttr("pageList", page);
		render("list.html");
	}

	@Clear
	public void tags_name(){
		List<String> tagsList = Tags.dao.findAllNameList();
		renderJson(tagsList);
	}
}
