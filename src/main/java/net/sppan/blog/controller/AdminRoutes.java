package net.sppan.blog.controller;

import com.jfinal.config.Routes;

import net.sppan.blog.controller.admin.AdminBlogController;
import net.sppan.blog.controller.admin.AdminController;
import net.sppan.blog.controller.admin.AdminResController;
import net.sppan.blog.controller.admin.AdminRoleController;
import net.sppan.blog.controller.admin.AdminTagsController;
import net.sppan.blog.controller.admin.AdminUserController;

public class AdminRoutes extends Routes{

	@Override
	public void config() {
		add("/admin", AdminController.class);
		add("/admin/sys/res", AdminResController.class);
		add("/admin/sys/role", AdminRoleController.class);
		add("/admin/sys/user", AdminUserController.class);
		add("/admin/content/blog", AdminBlogController.class);
		add("/admin/content/tags", AdminTagsController.class);
	}

}
