package net.sppan.blog.controller.web;

import java.util.HashMap;
import java.util.List;

import net.sppan.blog.controller.BaseController;
import net.sppan.blog.model.Blog;
import net.sppan.blog.model.Tags;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.jfinal.plugin.ehcache.CacheName;

public class BlogController extends BaseController {
	
	@Before(CacheInterceptor.class)
	@CacheName("blog")
	public void index(){
		int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 5);
        String search = getPara("search");
        
        Page<Blog> page;
        //如果是标签查询博客列表，则不使用关键字查询
        String searchTag = getPara("searchTag");
        if(StrKit.notBlank(searchTag)){
        	setAttr("searchTag", searchTag);
        	page = Blog.dao.pageNoContentByTag(pageNumber, pageSize, searchTag);
        }else{
            HashMap<String, Object> parameter = new HashMap<String, Object>();
            if(StrKit.notBlank(search)){
            	parameter.put("keyword", search);
            	setAttr("search", search);
            }
        	parameter.put("status", 0);
        	page = Blog.dao.pageNoContent(pageNumber, pageSize, parameter);
        }
		setAttr("pageList", page);
		render("index.html");
	}
	
	public void article(){
		Integer id = getParaToInt();
		Blog blog = Blog.dao.findFullById(id);
		setAttr("blog", blog);
		
		List<String> tags = Tags.dao.findNamesByBlogId(id);
		setAttr("tags", tags);
        render("article.html");
	}
	
}
