package net.sppan.blog.controller.web;

import net.sppan.blog.controller.BaseController;

public class IndexController extends BaseController {
	
	public void index(){
        redirect("/blog/");
	}
}
