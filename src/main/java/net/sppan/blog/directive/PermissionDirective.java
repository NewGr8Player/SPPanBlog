package net.sppan.blog.directive;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import net.sppan.blog.holder.ThreadLocalHolder;
import net.sppan.blog.model.Res;
import net.sppan.blog.model.User;
import net.sppan.blog.utils.WebUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class PermissionDirective implements TemplateDirectiveModel {

	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		if(params != null && params.containsKey("key")){
			String key = params.get("key").toString();
			User currentUser = WebUtils.currentUser(ThreadLocalHolder.getRequest(), ThreadLocalHolder.getResponse());
			Set<String> allResKeySet = Res.dao.getUserAllResKeySet(currentUser.getId());
			if(allResKeySet.contains(key)){
				body.render(env.getOut());
			}
		}
	}

}
