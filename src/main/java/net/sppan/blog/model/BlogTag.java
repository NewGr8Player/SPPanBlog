package net.sppan.blog.model;

import net.sppan.blog.model.base.BaseBlogTag;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class BlogTag extends BaseBlogTag<BlogTag> {
	public static final BlogTag dao = new BlogTag();
}
