package net.sppan.blog.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.IDataLoader;

import net.sppan.blog.commons.Constants;
import net.sppan.blog.model.base.BaseTags;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Tags extends BaseTags<Tags> {
	public static final Tags dao = new Tags();

	/**
	 * 获取所有标签的名称列表
	 * @return
	 */
	public List<String> findAllNameList() {
		return CacheKit.get(Constants.CacheName.top.get(), "_topTags", new IDataLoader() {
			
			@Override
			public List<String> load() {
				return Db.query("SELECT tag_name FROM sys_tags");
			}
		});
	}

	/**
	 * 根据博客ID删除关联的标签
	 * @param blogId
	 */
	public Integer deleteByBlogId(Integer blogId) {
		return Db.update("DELETE FROM sys_blog_tag WHERE blog_id = ?",blogId);
	}

	/**
	 * 根据标签名称查找标签
	 * @param tagName
	 * @return
	 */
	public Tags findByTagName(String tagName) {
		return findFirst("SELECT * FROM sys_tags WHERE tag_name = ?",tagName);
	}

	/**
	 * 根据博客ID获取关联的标签名称
	 * @param id
	 * @return
	 */
	public List<String> findNamesByBlogId(Integer id) {
		return Db.query("SELECT tag_name FROM sys_tags AS t,sys_blog_tag b WHERE t.id = b.tag_id AND b.blog_id = ?",id);
	}
}
